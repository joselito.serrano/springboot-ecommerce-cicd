package MDI_Novare.Ecommerce.Controller;

import MDI_Novare.Ecommerce.Repository.Product;
import MDI_Novare.Ecommerce.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping("/add")
    public Product addUser(@RequestBody Product product) {
        return productService.save(product);
    }

    @GetMapping("/getAll")
    public List<Product> getAllProduct() {
        return productService.findAll();
    }

    @GetMapping("/getById")
    public Product getById(@RequestParam long id) {
        return productService.findById(id);
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable long id) {
        productService.deleteById(id);
    }

    @PutMapping("/updateById/{id}")
    public Product updateById(@PathVariable long id, @RequestBody Product product) {
        return productService.updateById(id, product);
    }
}
